# Wizztodon [FR]
![](https://framagit.org/troll/wizztodon/-/raw/master/Wizz_logo_96.png)

Comme nous sommes un peu masochistes, nous avons ressuscité le **WIZZ MSN** pour l'intégrer dans Mastodon!  
Il vous suffit d'installer l'extension "**wizztodon-version-an+fx.xpi**" sur votre navigateur pour que vos **#wizz** prennent vie \^__^/  
Ne nous remerciez pas!  

Recomandation: pour un maximum de nuisance pensez à utiliser cette extension dans un open-space.


## Comment jouir de cette fabuleuse extension ?
1. Télécharger l’extension "wizztodon-version-an+fx.xpi" qui se trouve dans le dossier (build) ci-dessus. Prennez la dernière version.
2. Glisser le fichier "wizztodon-version-an+fx.xpi" au niveau des onglet dans Firefox
3. Une popup s'ouvre pour demander si vous acceptez l'installation, cliquez pour l'installer.
4. Rafraîchir l'onglet Mastodon après l'installation en pressant sur (F5)
5. Vous pouvez désormais aller vous faire #wizz !!!

## Comment désinstaller cette fabuleuse extension ?
1. Il suffit de vous rendre dans votre menu Firefox > Extensions , repérer l'extension et la désinstaller.
2. Vous pouvez également simplement rendre silencieux votre onglet Firefox avec un clic-droit sur l'onglet puis choisissez: "Rendre l'onglet muet"

## Astuces
- N'hésitez pas à indiquer dans votre profil mastodon que vous êtes #wizzable afin que vos contacts sachent qu'ils peuvent vous #wizzer
- Le fonctionnement actuel de l'extension ajoute une surveillance de #wizz sur chacune des colonnes que vous avez d'épinglées au moment de l'ouverture de la page mastodon (quelques secondes après en réalité), n'hésitez pas à faire une recherche #wizz, à épingler cette colonne puis à rafraichir la page si vous avez le sommeil trop facile ou que vous êtes trop concentré sur votre travail


# Wizztodon [EN]
![](https://framagit.org/troll/wizztodon/-/raw/master/Wizz_logo_96.png)

As we are a bit masochistic, we have resurrected the **WIZZ MSN** to integrate it into Mastodon!  
Just install the extension "**wizztodon-version-an+fx.xpi**" on your browser to make your **#wizz** come alive \^__^/  
Don't thank us!  

Recommendation: for a maximum of nuisance, think about using this extension in an open-space.


## How to enjoy this fabulous extension?
1. Download the extension "wizztodon-version-an+fx.xpi" which is located in the build folder above. Take the latest version.
2. Drag the file "wizztodon-version-an+fx.xpi" to the tabs in Firefox
3. A popup opens to ask if you accept the installation, click to install it.
4. Refresh the Mastodon tab after installation by pressing (F5)
5. #wizz!!!!!

## How to uninstall this fabulous extension?
1. Simply go to your Firefox > Extensions menu, locate the extension and uninstall it.
2. You can also simply mute your Firefox tab with a right-click on the tab and then choose: "Mute the tab"

## Tips
- Do not hesitate to put #wizzable in your mastodon profile so your contacts know you can be #wizzed
- Actual implementation watch over columns wich are automaticaly openned when you open mastodon, if you easily fall asleep or you have problem with over-focusing on your work, you can remedy this by searching #wizz, pin the column and refresh mastodon's page

**Auteurs:**  
*@ronane@mamot.fr*  
*@troll@maly.io*  