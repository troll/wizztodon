// Fonction pour jouer un son
function playSound() {
  const audio = new Audio(browser.runtime.getURL('nudge.mp3'));
  audio.play();
}

// Set pour garder une trace des éléments déjà traités
const processedElements = new Set();
let soundPlayed = false;

// Fonction pour ajouter la classe de vibration à l'élément de classe '.status' contenant 'wizz'
function vibrateWizzElements(entries, observer) {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      const element = entry.target;
      if (element.innerText.includes('wizz') && !processedElements.has(element)) {
        element.classList.add('vibrate');
        // Ajouter l'élément au Set des éléments traités
        processedElements.add(element);
        if (!soundPlayed) {
          playSound();
          soundPlayed = true;
          // Réinitialiser le flag après 3 secondes
          setTimeout(() => {
            soundPlayed = false;
          }, 3000); // 3000 millisecondes = 3 secondes
        }
      }
    }
  });
}

// Création de l'observateur IntersectionObserver
const observer = new IntersectionObserver(vibrateWizzElements);

// Observer les éléments de classe '.status'
document.querySelectorAll('.status').forEach(element => {
  observer.observe(element);
});

// Observer pour surveiller les changements de la DOM
const domObserver = new MutationObserver(() => {
  document.querySelectorAll('.status').forEach(element => {
    if (!processedElements.has(element)) {
      observer.observe(element);
    }
  });
});

// Configurer l'observer pour surveiller les ajouts de noeuds
domObserver.observe(document.body, {
  childList: true,
  subtree: true,
});